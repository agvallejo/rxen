//! Freestanding Xen interface
//!
//! Implementation of [`XenInterface`] for baremetal systems. This is strictly
//! meant for kernel-code, as it communicates directly with the hypervisor.

/// Abstraction of the Xen hypercall interface. It exists to preserve
/// the same semantics in both hosted and freestanding environments, but it's
/// zero-cost in freestanding.
pub struct XenInterface;

use crate::{
    hyp::{IsDomCtl, IsSysCtl},
    XenCall,
};

impl XenCall for XenInterface {
    fn try_new() -> Option<Self> {
        Some(Self)
    }
    unsafe fn sysctl<T: IsSysCtl>(&self, _t: &mut T) -> Result<(), i32> {
        todo!("not_done");
    }
    unsafe fn domctl<T: IsDomCtl>(&self, _t: &mut T) -> Result<(), i32> {
        todo!("not_done");
    }
}
