//! Hypercall handling
//!
//! Abstractions over the mechanism by which hypercalls are sent.
//!
//! If hosted: `/dev/xen/privcmd` is used via `ioctl()`.
//! If freestanding: A direct hypercall is issued.

#[cfg(target_family = "unix")]
pub mod unix;
#[cfg(target_family = "unix")]
pub use unix::XenInterface;

#[cfg(target_os = "none")]
pub mod none;
#[cfg(target_os = "none")]
pub use none::XenInterface;

/// The common contract for [`unix::XenInterface`] and [`none::XenInterface`].
/// This trait allows both implementations to remain in sync.
pub trait XenCall
where
    Self: Sized,
{
    /// Attempt to create a hypercall interface
    ///
    /// This is fallible in a hosted setting (e.g: due to lack of permissions),
    /// but always succeeds in freestanding mode.
    fn try_new() -> Option<Self>;

    /// Issue a `sysctl` hypercall
    ///
    /// # Safety
    /// The caller must ensure `t` is wellformed, and that the side effects of
    /// the hypercall can't interfere with Rust's memory model.
    unsafe fn sysctl<T: IsSysCtl>(&self, t: &mut T) -> Result<(), i32>;

    /// Issue a `domctl` hypercall
    ///
    /// # Safety
    /// The caller must ensure `t` is wellformed, and that the side effects of
    /// the hypercall can't interfere with Rust's memory model.
    unsafe fn domctl<T: IsDomCtl>(&self, t: &mut T) -> Result<(), i32>;
}

/// Dumb trait to identify buffers typed as `sysctl`. This adds some
/// type-safety by statically preventing sending a `domctl` buffer as a
/// `sysctl`, for example.
pub trait IsSysCtl {}

/// Dumb trait to identify buffers typed as `domctl`. This adds some
/// type-safety by statically preventing sending a `domctl` buffer as a
/// `sysctl`, for example.
pub trait IsDomCtl {}

/// Fixed number identifying a hypercall as a `sysctl`
const SYSCTL_CMD: u32 = 35;

/// Fixed number identifying a hypercall as a `domctl`
const DOMCTL_CMD: u32 = 36;
