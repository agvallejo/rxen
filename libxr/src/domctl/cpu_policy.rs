//! Binding for the `xen_domctl_cpu_policy` buffer type
//!
//! Enables the `XEN_DOMCTL_{get,set}_cpu_policy` hypercalls

use crate::{
    hyp::{IsDomCtl, XenCall, XenInterface},
    DomId,
};

use super::DomCtlHeader;

/// Buffer type for getting/setting the CPU policy of a domain.
///
/// The CPU policy is a CPUID and MSR based description of the features exposed
/// to the VM.
#[repr(C)]
#[derive(Debug)]
pub struct CpuPolicyRaw {
    /// Common header to every `sysctl`. See [`DomCtlHeader`].
    pub hdr: DomCtlHeader,
    pub nr_leaves: u32,
    pub nr_msrs: u32,
    pub leaves: u64,
    pub msrs: u64,
    pub err_leaf: u32,
    pub err_subleaf: u32,
    pub err_msr: u32,
    pub _pad0: [u8; 88],
}

impl CpuPolicyRaw {
    /// Identifies which particular `sysctl` this hypercall is
    const DOMCTL_OP_GET: u32 = 82;
    const DOMCTL_OP_SET: u32 = 83;

    /// Reads the hypervisor ring console
    ///
    /// # Safety
    /// Same as [`XenCall::sysctl`]
    pub unsafe fn get(&mut self, domid: DomId, xi: &XenInterface) -> Result<(), i32> {
        self.hdr.domid = domid;
        self.hdr.op = Self::DOMCTL_OP_GET;
        xi.domctl(self)
    }

    /// Reads the hypervisor ring console
    ///
    /// # Safety
    /// Same as [`XenCall::sysctl`]
    pub unsafe fn set(&mut self, domid: DomId, xi: &XenInterface) -> Result<(), i32> {
        self.hdr.domid = domid;
        self.hdr.op = Self::DOMCTL_OP_SET;
        xi.domctl(self)
    }
}

use core::mem::size_of;
const _: () = assert!(size_of::<CpuPolicyRaw>() - size_of::<DomCtlHeader>() == 128);

impl IsDomCtl for CpuPolicyRaw {}
