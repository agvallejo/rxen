//! Binding for the `xen_domctl_createdomain` buffer type
//!
//! Enables the `XEN_DOMCTL_createdomain` hypercalls

use crate::{
    hyp::{IsDomCtl, XenCall, XenInterface},
    DomId,
};

use super::DomCtlHeader;

/// Buffer type for creating new domains
#[repr(C)]
#[derive(Debug)]
pub struct CreateDomainRaw {
    /// Common header to every `sysctl`. See [`DomCtlHeader`].
    pub hdr: DomCtlHeader,
    /// XSM security label.
    pub ssidref: u32,
    /// UUID of the domain. Only relevant to assist toolstack.
    pub handle: [u8; 16],
    /// Arch-agnostic switches for functionality available to the new domain
    /// 
    /// bit0: is_hvm
    /// bit1: is_hap
    /// bit2: checkmem_after_s3 (?)
    /// bit3: has_out_of_sync_shadow_ptab (?)
    /// bit4: is_xenstore_domain
    /// bit5: has_iommu
    /// bit6: has_nested_virt
    /// bit7: has_vpmu
    pub flags: u32,
    /// ?
    pub iommu_opts: u32,
    /// Maximum number of vCPUs the domain is expected to have. This includes
    /// hotplugged vCPUs.
    pub max_vcpus: u32,
    /// Maximum number of event channel ports that may be attached to the domain.
    pub max_evtchn_port: u32,
    /// ? (< 0 means "use default in Xen")
    pub max_grant_frames: i32,
    /// ? (< 0 means "use default in Xen")
    pub max_maptrack_frames: i32,
    /// ?
    pub grant_opts: u32,
    /// ?
    pub vmtrace_size: u32,
    /// ?
    pub cpupool_id: u32,
    // TODO: Other architectures
    #[cfg(target_arch = "x86_64")]
    pub emulation_flags: u32,
}

impl CreateDomainRaw {
    /// Identifies which particular `sysctl` this hypercall is
    const DOMCTL_OP: u32 = 82;

    /// Creates a domain 
    ///
    /// # Safety
    /// Same as [`XenCall::sysctl`]
    pub unsafe fn create(&mut self, domid: DomId, xi: &XenInterface) -> Result<(), i32> {
        self.hdr.domid = domid;
        self.hdr.op = Self::DOMCTL_OP;
        xi.domctl(self)
    }
}

use core::mem::size_of;
const _: () = assert!(size_of::<CreateDomainRaw>() - size_of::<DomCtlHeader>() == 128);

impl IsDomCtl for CreateDomainRaw {}
