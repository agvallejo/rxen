//! Abstraction over `sysctl` Xen hypercalls

pub mod readconsole;
pub use readconsole::ReadConsoleRaw;

/// Header common to every `sysctl` (See [`ReadConsole`], for example). This is
/// the common header that identifier the operation and the layout of the passed
/// data.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct SysCtlHeader {
    /// Identifies which particular `sysctl` must be performed and
    /// which variant follows this header. The hypervisor is meant to read this
    /// value and find the meaning of the bits that follow it (See
    /// [`ReadConsole`] for an example. Think single-level inheritance.
    op: u32,
    /// Must match [`SysCtlHeader::INTERFACE_VERSION`]. Used to tell the
    /// hypervisor the expected ABI.
    interface_version: u32,
}

impl SysCtlHeader {
    /// Xen doesn't have a stable ABI and bumps this number whenever the ABI
    /// changes. When a hypercall lands on the hypervisor, it compares its own
    /// internal number with this one, and will fail early with `-EACCES` if
    /// they don't match.
    const INTERFACE_VERSION: u32 = 0x15;

    /// Creates a valid `sysctl` header for a specific `op`
    pub const fn new() -> Self {
        let interface_version = Self::INTERFACE_VERSION;
        Self {
            op: u32::MAX, // Start with a poison value
            interface_version,
        }
    }
}
